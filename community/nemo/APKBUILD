# Contributor: Clayton Craft <clayton@craftyguy.net>
maintainer="Hugo Osvaldo Barrera <hugo@whynothugo.nl>"
pkgname=nemo
pkgver=6.4.2
pkgrel=0
pkgdesc="File manager for the Cinnamon desktop environment"
# s390x and ppc64le blocked by exempi
# riscv64 disabled due to missing rust in recursive dependency
arch="all !s390x !ppc64le !riscv64"  # exempi, libexif-dev not available for all archs
url="https://github.com/linuxmint/nemo"
license="GPL-2.0-only"
depends="desktop-file-utils"
makedepends="
	cinnamon-desktop-dev
	dconf-dev
	exempi-dev
	gobject-introspection-dev
	intltool
	json-glib-dev
	libexif-dev
	libgsf-dev
	libnotify-dev
	libxml2-dev
	meson
	python3
	xapp-dev
	"
checkdepends="xvfb-run"
source="https://github.com/linuxmint/nemo/archive/$pkgver/nemo-$pkgver.tar.gz"
subpackages="$pkgname-doc $pkgname-dev"
options="!check" # tests are broken: https://github.com/linuxmint/nemo/issues/2501

build() {
	abuild-meson . output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

check() {
	xvfb-run meson test -C output
}
sha512sums="
e1a68b8341b936d4ef8e5bc19b9b0c26f0e01cf08e007cb6be90d782e93a13fc4850c0592ca5c1d02c84035c8d98ef0ded8c1efa5f7caac88e60487898db64e0  nemo-6.4.2.tar.gz
"
